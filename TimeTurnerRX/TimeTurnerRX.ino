/*
 *  Project: TimeTurnerRX
 *  
 *  Description:
 *    A simple microcontroller based system to track how long two medicine bottles
 *    have been in place, which implies how long its beeen since you took your
 *    medications. I built this for my own basic use instead of buying one of the
 *    *many* available medication tracking & reminder systems on the market.
 *    This is plenty of informatino and tracking for my needs.
 *  
 *  Contributors:
 *    Aaron S. Crandall <acrandal@gmail.com>, 2020
 *    
 *  Copyright:
 *    Attribution-NonCommercial-ShareAlike 4.0 International
 *    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * 
 * 
 * 
 */

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

unsigned long g_curr_time;
unsigned long g_time1;
unsigned long g_time2;
unsigned long g_time1_timeout;
unsigned long g_time2_timeout;
int g_last_button1;
int g_last_button2;

#define BTN1 8
#define BTN2 9

#define BOTTLE_ABSCENT_TIMEOUT 10000


void setup() {
  Serial.begin(9600);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);

  display.clearDisplay();
  display.display();

  g_last_button1 = HIGH;
  g_last_button2 = HIGH;

  g_curr_time = millis();
  g_time1 = millis();
  g_time2 = millis();
  delay(1000);
}

void loop() {
  updateTimes();
  updateDisplay();
  delay(100);
}


// ** Update current time **
void updateTimes(void) {
  g_curr_time = millis();

  // ** For button 1 (left bottle) **
  if( digitalRead(BTN1) == HIGH && g_last_button1 == LOW ) {
    g_last_button1 = HIGH;
    g_time1_timeout = g_curr_time;
  } else if( digitalRead(BTN1) == LOW ) {
    g_last_button1 = LOW;
  }

  if( g_last_button1 == HIGH && (g_curr_time - g_time1_timeout) > BOTTLE_ABSCENT_TIMEOUT ) {
    g_time1 = g_curr_time;
  }

  // ** For button 2 (right bottle) **
  if( digitalRead(BTN2) == HIGH && g_last_button2 == LOW ) {
    g_last_button2 = HIGH;
    g_time2_timeout = g_curr_time;
  } else if( digitalRead(BTN2) == LOW ) {
    g_last_button2 = LOW;
  }

  if( g_last_button2 == HIGH && (g_curr_time - g_time2_timeout) > BOTTLE_ABSCENT_TIMEOUT ) {
    g_time2 = g_curr_time;
  }
}


// ** Update the display **
void updateDisplay(void) {
  display.clearDisplay();

  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,8);             // Start at top-left corner
  display.setTextSize(2);             // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);

  float hours1 = (float)(g_curr_time - g_time1) / 3600000.0;
  display.println(hours1, 3);
  
  display.setCursor(0,16*3);             // Start at top-left corner
  float hours2 = (float)(g_curr_time - g_time2) / 3600000.0;

  int indent = 10 - 5;    // 10 characters wide screen, 5 min digits printing
  if( hours2 > 9 ) { indent--; }
  if( hours2 > 99 ) { indent--; }
  if( hours2 > 999 ) { indent--; }

  for( int i = 0; i < indent; i++ ) {
    display.print(" ");
  }
  
  display.println(hours2, 3);


  display.drawLine(display.width() - 1, display.height() / 2, 0, display.height() / 2, SSD1306_WHITE);
  display.display();
}
