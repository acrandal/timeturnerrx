# TimeTurnerRX 

#### Description:  
 A simple microcontroller based system to track how long two medicine bottles
 have been in place, which implies how long its beeen since you took your
 medications. I built this for my own basic use instead of buying one of the
 *many* available medication tracking & reminder systems on the market.
 This is plenty of informatino and tracking for my needs.
 
#### Contributors:  
  Aaron S. Crandall <acrandal@gmail.com>, 2020

#### Copyright:  
  Attribution-NonCommercial-ShareAlike 4.0 International  
  http://creativecommons.org/licenses/by-nc-sa/4.0/

![Image of Working System](photos/TimeTurnerRX.jpg "TimeTurnerTX")
